resource "aws_s3_bucket" "bb" {
  bucket = "my-tf-test-bucket"
  acl    = "public-read"
  tags = {
    Name        = "My bucket here update"
    Environment = "Dev"
  }
}
